from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import os
package_path = os.path.abspath(__file__ + "/../../")
sys.path.insert(0, package_path)

import logging
from EMCqMRI.core.engine import build_model as core_build
from utilities import all_utils as utils

def override_configuration(config_object):
    
    # DATASET MODEL
    from custom_dataset import dti_dataset
    config_object.args.engine.dataset_model = dti_dataset.DatasetModel(config_object)
    logging.info('Loading custom dataset model: {}'.format(config_object.args.engine.datasetModel))
        

    # SIGNAL MODEL
    from custom_signal_model import dti_model
    config_object.args.engine.signal_model = dti_model.DTI(config_object)
    logging.info('Loading custom signal model: {}'.format(config_object.args.engine.signalModel))


    # INFERENCE MODEL
    from custom_inference_model import rim
    config_object.args.engine.inference_model = rim.Rim(config_object)
    logging.info('Loading custom inference model: {}'.format(config_object.args.engine.inferenceModel))

    # TRAINING LOSS
    from custom_loss import dwi_loss
    config_object.args.engine.objective_fun = dwi_loss.dwiLoss(config_object)
    logging.info('Loading custom loss function model: {}'.format(config_object.args.engine.lossFunction))


    # LIKELIHOOD MODEL
    from custom_likelihood_model import rician_model
    config_object.args.engine.likelihood_model = rician_model.Rician(config_object)
    logging.info('Loading custom likelihood model: {}'.format(config_object.args.engine.likelihoodModel))

    VIZUALIZATION
    if config_object.args.engine.writeTensorboard:
        config_object.args.engine.log_training_fun = utils.TensorboardLog(config_object)
        logging.info('Using Tensorboard logging function: {}'.format(config_object.args.engine.log_training_fun))
    else:
        config_object.args.engine.log_training_fun = utils.LogFun(config_object)
        logging.info('Loading local logging function: {}'.format(config_object.args.engine.log_training_fun))


    # EXTRA CONFIGURATIONS
    config_object.args.engine.prepare_batch = utils.custom_prep_batch
    config_object.args.engine.compute_testing_loss = True

    return config_object


if __name__=='__main__':

    configurationObj = core_build.make('training', override_configuration)
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))

    configurationObj.args.engine.trainer.run()