
from dipy.reconst.dti import fractional_anisotropy, color_fa, mean_diffusivity, radial_diffusivity, axial_diffusivity
from dipy.reconst.dti import lower_triangular, eig_from_lo_tri
from dipy.viz import window, actor
import dipy.data as dpd
from EMCqMRI.core.utilities import image_utilities
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt
import nibabel as nii
import os
import numpy as np
import torch



def build_d_mat(tensor):
    d_mat = np.array([[tensor[0], tensor[1], tensor[2]],
                    [tensor[1], tensor[3], tensor[4]],
                    [tensor[2], tensor[4], tensor[5]]])
    d_mat_perm = np.transpose(d_mat, (-2, -1, 0, 1))
    return d_mat_perm


def compute_eigen(tensor):
    d_mat = build_d_mat(np.stack(tensor))
    l_triang = lower_triangular(d_mat)
    eig_v = eig_from_lo_tri(l_triang, min_diffusivity=1e-6)
    e_val = eig_v[...,:3]
    e_vec = eig_v[...,3:12]
    sh_tensor = tensor.shape[-2:]
    e_vec = e_vec.reshape(*sh_tensor, 3, 3)

    e_val = e_val.transpose(1,0,2)
    e_vec = e_vec.transpose(1,0,2,3)

    return e_val, e_vec


def get_color_fa(fa, evec):
    color_bar = color_fa(fa, evec)
    return color_bar


def compute_fa_md(param):
    e_val, e_vec = compute_eigen(param)
    fa_map = fractional_anisotropy(e_val)
    md_map = mean_diffusivity(e_val)
    return fa_map, md_map

def threshold_fa_map(fa, thres):
    fa[fa<thres]=0
    return fa


def load_data(path, basename, ext):
    path_param = os.path.join(path, basename + '_parameters.'+ext)
    path_signal = os.path.join(path, basename + '_measured_signal.'+ext)
    path_fitted_signal = os.path.join(path, basename + '_fitted_signal.'+ext)


    if ext=='pkl':
        try:
            with open(path_param, 'rb') as f:
                param_est = pickle.load(f)
        except:
            print("No estimated parameter file")

        try:
            with open(path_signal, 'rb') as f:
                signal = pickle.load(f)
        except:
            print("No signal file")

        try:
            with open(path_fitted_signal, 'rb') as f:
                predicted_signal = pickle.load(f)
        except:
            print("No predicted signal file")

    elif ext=='nii':
        try:
            data_obj = nii.load(path_param)
            param_est = np.asarray(data_obj.dataobj)
        except:
            print("No estimated parameter file")
            param_est = None

        try:
            data_obj = nii.load(path_signal)
            signal = np.asarray(data_obj.dataobj)
        except:
            print("No signal file")
            signal = None
            
        try:
            data_obj = nii.load(path_fitted_signal)
            predicted_signal = np.asarray(data_obj.dataobj)
        except:
            print("No predicted signal file")
            predicted_signal = None
    param_est = np.moveaxis(param_est, -1,0)
    signal = np.moveaxis(signal, -1,0)
    predicted_signal = np.moveaxis(predicted_signal, -1,0)
    return param_est, signal, predicted_signal



def make_mask(s0_image):
    mask = []
    return mask


def flip_parameters(parameters):
    flipped_parameters = []
    return flipped_parameters
