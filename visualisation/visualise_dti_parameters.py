import argparse
from dipy.viz import window, actor
import dipy.data as dpd
from EMCqMRI.core.utilities import image_utilities
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import ntpath
import numpy as np
import torch
import utilities


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


class Visualisation(object):
    def __init__(self):
        colors = ['blue', 'black', 'red']
        self.cm = LinearSegmentedColormap.from_list('error_map', colors, N=100)
        
    def set_options(self, options):
        self.options = options
        
    def vis_fa_md_maps(self, fa_map, md_map, mask=[]):
        fig, ax = plt.subplots(1,2)
        ax[0].set_title('Fractional Anisotropy')
        ax[1].set_title('Mean Diffusivity')
        
        if mask:
            fa_map = fa_map*mask
            md_map = md_map*mask

        # fa_map = np.flip(fa_map,0)
        # md_map = np.flip(md_map,0)

        im1 = ax[0].imshow(fa_map, vmin=0, vmax=1, cmap='gray')
        ax[0].set_xticks([])
        ax[0].set_yticks([])
        divider = make_axes_locatable(ax[0])
        cax = divider.append_axes('right', size='5%', pad=0.05)
        cbar = fig.colorbar(im1, cax=cax, orientation='vertical')
        cbar.set_label('Fractional Anisotropy', rotation=90)
        

        im2 = ax[1].imshow(md_map, vmin=0, vmax=2, cmap='gray')
        ax[1].set_xticks([])
        ax[1].set_yticks([])
        divider = make_axes_locatable(ax[1])
        cax = divider.append_axes('right', size='5%', pad=0.05)
        cbar = fig.colorbar(im2, cax=cax, orientation='vertical')
        cbar.set_label(r'Mean Diffusivity [$\mu m ^2/ ms$]', rotation=90)
        fig.tight_layout()
        plt.show()

    def vis_eigenvalues(self, e_val, mask=[]):
        titles = [r'$\lambda_1$', r'$\lambda_2$', r'$\lambda_3$']
        fig, ax = plt.subplots(1,3)
        eval_ = np.transpose(e_val, (-1, 0, 1))
        fig.suptitle("Predicted eigenvalues")
        for i, ev in enumerate(eval_):
            # ev = np.flip(ev, 0)
            ax[i].set_title(titles[i])
            im1 = ax[i].imshow(ev, vmin=0, vmax=1.5, cmap='gray')

            divider = make_axes_locatable(ax[i])
            cax = divider.append_axes('right', size='5%', pad=0.05)
            cbar = fig.colorbar(im1, cax=cax, orientation='vertical')
            ax[i].set_xticks([])
            ax[i].set_yticks([])

        fig.tight_layout()
        plt.show()


    def vis_dce_maps(self, fa, e_vec, mask=[]):
        if mask:
            fa = fa*mask
        color_bar = utilities.get_color_fa(fa, e_vec)
        color_bar = np.flip(color_bar, 0)
        fa = np.flip(fa, 0)

        plt.figure()
        plt.title('Direction Color Encoded FA maps')
        plt.imshow(fa, origin='lower', cmap='gray', alpha=0.8)
        plt.xticks([])
        plt.yticks([])
        plt.imshow(color_bar, origin='lower', alpha=0.8)
        plt.show()

    def vis_3d_tensor_map(self, fa, e_val, e_vec, mask=[]):
        fa = utilities.threshold_fa_map(fa, 0.15)
        color_bar = utilities.get_color_fa(fa, e_vec)
        color_bar = np.flip(np.transpose(color_bar, (1,0,2)),1)
        fa = np.flip(np.transpose(fa, (1,0)),1)

        e_val = np.flip(e_val[:,:,None,...].transpose(1,0,2,3),1)
        e_vec = np.flip(e_vec[:,:,None,...].transpose(1,0,2,3,4),1)
        color_bar = color_bar[:,:,None,...]
        sphere = dpd.default_sphere
        ren = window.Scene()
        ren.add(actor.tensor_slicer(e_val, e_vec, scalar_colors=color_bar, sphere=sphere,
                                    scale=0.6))
        window.show(ren)

    def vis_signal_fit(self, signal, predicted_signal, mask=[]):
        image_utilities.imagebrowse_slider(signal, cube2=predicted_signal, vmin_=0, vmax_=predicted_signal.max()/2)
        
    def __call__(self):
        parameters, signal, predicted_signal = utilities.load_data(self.options['path'], self.options['basename'], self.options['file_extension'])
        fa_map, md_map = utilities.compute_fa_md(parameters[1:])
        eigen_val, eigen_vec = utilities.compute_eigen(parameters[1:])
        
        if signal is not None:
            mask = utilities.make_mask(signal[0])
        else:
            mask = []

        if self.options['vis_fa_md']: 
            print("Plotting FA and MD maps")
            self.vis_fa_md_maps(fa_map, md_map)

        if self.options['vis_eigenvalues']:
            print("Plotting predicted eigenvalues")
            self.vis_eigenvalues(eigen_val)

        if self.options['vis_signal_fitting']:
            print("Plotting signal fitting")
            if signal is not None:
                self.vis_signal_fit(signal, predicted_signal)
            else:
                print("No signal file provided. Unable to evaluate the signal fitting")

        if self.options['vis_dce_map']:
            print("Plotting Direction Color Encoded FA maps")
            self.vis_dce_maps(fa_map, eigen_vec)

        if self.options['vis_3d_tensors']:
            print("Plotting 3d representation of the estimated tensors")
            self.vis_3d_tensor_map(fa_map, eigen_val, eigen_vec)


if __name__=='__main__':

    command_parser = argparse.ArgumentParser(description='Visualisation for dtiRIM')
    command_parser.add_argument('-data_path', type=str, help='Path to estimated DTI parameters')
    command_parser.add_argument('-base_filename', type=str, help='Base filename - same as name of the DWI-MRI data, without extension')
    command_parser.add_argument('-file_type', type=str, default='nii', help='Type of file (nii or pkl)')
    command_parser.add_argument('--vis_fa_md', type=str2bool, default=False, help='True to visualise the Fractional Anisotropy and Mean Diffusivity maps')
    command_parser.add_argument('--vis_dce_map', type=str2bool, default=False, help='True to visualise the Direction Color Encoded FA maps')
    command_parser.add_argument('--vis_signal_fitting', type=str2bool, default=False, help='True to visualise the signal fitting')
    command_parser.add_argument('--vis_eigenvalues', type=str2bool, default=False, help='True to visualise the eigenvalues of estimated tensor')
    command_parser.add_argument('--vis_3d_tensors', type=str2bool, default=False, help='True to visualise the 3d representation of the estimated tensors, color encoded by direction')
    args = command_parser.parse_args()

    options = {'vis_fa_md': args.vis_fa_md,
               'vis_dce_map': args.vis_dce_map,
               'vis_signal_fitting': args.vis_signal_fitting,
               'vis_eigenvalues': args.vis_eigenvalues,
               'vis_3d_tensors': args.vis_3d_tensors,
               'file_extension': args.file_type,
               'path': args.data_path,
               'basename': args.base_filename}

    vis_class = Visualisation()
    vis_class.set_options(options)
    vis_class()
