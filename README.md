
# **dtiRIM**: A generalisable deep learning method for Diffusion Tensor Imaging

The dtiRIM is a deep learning method to estimate diffusion tensor parameters from Diffusion Weighted MRI. Because the dtiRIM generalises well to unseen data, a single trained model is effective even when the testing data is very different from the training data.

This repository offers all modules required to use the dtiRIM, including a pre-trained network model.

## Installation
The dtiRIM was tested with Python 3.x. The steps below install all dependencies required to use the dtiRIM, notably [PyTorch](https://pytorch.org/) and [EMCqMRI](https://gitlab.com/e.ribeirosabidussi/emcqmri).

1. Clone this repository: ```git clone https://gitlab.com/radiology/quantitative-mri/emcqmri_dti```
2. Enter new folder: ```cd emcqmri_dti```
2. Create a virtual environment: ```python3 -m venv dtiRIM_venv```
3. Activate the virtual environment: ```. ./dtiRIM_venv/bin/activate```
4. Update the pip package: ```pip3 install --upgrade pip```
5. Install dependencies: ```pip3 install -r requirements.txt```

## **Checking installation: Running an example**

To verify if the dtiRIM and its dependencies were correctly set up in your system, you can run the example provided. By executing the python script:
```console
@user:~$ python3 estimate_dti.py --configurationFile custom_configuration/estimate/engine_configuration.txt
```
the dtiRIM will estimate the diffusion parameters from the example data in "./data/testing/".

### **The dtiRIM output**
The results will be saved in 2 separate files (in this example, inside the folder "./data/tmp_results/"):
- **dwi_parameters.nii** - Contains the estimated diffusion parameters (The non-diffusion weighted S<sub>0</sub>, and the unique elements of the diffusion tensor D<sub>xx</sub>, D<sub>xy</sub>, D<sub>xz</sub>, D<sub>yy</sub>, D<sub>yz</sub>, and D<sub>zz</sub>. The output data has shape [7, X, Y], with parameters in the same order as described above.
- **dwi_fitted_signal.nii** - Contains the fitted signal (created using the parameters above, the gradient file, and the Diffusion Tensor model). The output data has shape [N, X, Y] where N is the number of weighted images.
- **dwi_measured_signal.nii** -- Contains a copy of the DWI-MRI data used to infer the diffusion tensor parameters.



## **Visualising results**
We also provide a visualisation tool so you can check the parameter estimates of the dtiRIM. To visualise the results from the example, you can run

```console
@user:~$ python3 visualisation/visualise_dti_parameters.py -data_path ./data/tmp_results/ -base_filename dwi -file_type nii --vis_fa_md True --vis_signal_fitting True --vis_dce_map True --vis_eigenvalues True --vis_3d_tensors False
```
With arguments:
- **-data_path** (str): Path to folder containing the processed data.
- **-base_filename** (str): Same filename as your DWI-MRI data, without the file extension.
- **-file_type** (str): File extension of the DWI-MRI data.
- **--vis_fa_md** (bool): (Optional) If True, visualise the Fractional Anisotropy and Mean Diffusivity maps; Default False.
- **--vis_signal_fitting** (bool): (Optional) If True, visualise the signal fitting; Default False.
- **--vis_dce_map** (bool): (Optional) If True, visualise the Direction Color Encoded FA maps; Default False.
- **--vis_eigenvalues** (bool): (Optional) If True, visualise the eigenvalues of estimated tensor; Default False.
- **--vis_3d_tensors** (bool): (Optional) If True, visualise the 3d representation of the estimated tensors, color encoded by direction; Default False.

### 
# **Using the dtiRIM with my own data**



If you wish to use the dtiRIM with your own data, you can especify the data path (and saving path) in two ways:

1. Directly specifying in command line while calling the method:
    ```console
    @user:~$ python3 estimate_dti.py --configurationFile custom_configuration/estimate/engine_configuration.txt --testingDataPath PathToMyData/sub1 --saveResultsPath PathToSavedData/sub1
    ```

2. Modifying the [estimation configuration file](custom_configuration/estimate/engine_configuration.txt).
    
    The options "testingDataPath" and "saveResultsPath" should be modified to match your DW-MRI data folder and the folder where to save the results.

### **Gradient files**

The dtiRIM accepts '**.txt**' files, with bvecs and bvals stored as in the [MRTrix format](https://mrtrix.readthedocs.io/en/latest/concepts/dw_scheme.html#mrtrix-format). For example:

```txt
0.0 0.0 0.0 0.0
 0.70007 0.71407 -0.00377 1000.0
 0.67739 0.28219 -0.67935 1000.0
 0.31955 0.71991 -0.61613 1000.0
 0.26805 0.72521 -0.63445 1000.0
 -0.72775 0.4781 -0.49173 1000.0
 -0.69488 0.45142 0.55978 1000.0
```

Each line correspond to [ x y z b ], where [ x y z ] are the components of the gradient vector, and b is the b-value in units of s/mm². The first line indicates a non-diffusion weighted image.

### **Folder structure**

The data folder must include at least the image data and its corresponding gradient file. The dtiRIM follows a naming convention to link both files:

- **File**: FILE_NAME.ext
- **Gradient file**: FILE_NAME_grad_dir.txt

where the dtiRIM searches for the gradient file by appending '_grad_dir.txt' to the file name (without the file extension). The file type 'ext' can be either 'pkl' or 'nii'.

The processed files are saved as:
- FILE_NAME_parameters.ext
- FILE_NAME_fitted_signal.ext
- FILE_NAME_measured_signal.ext

where 'ext' will be the same as the input data.

If the folder contains more than 1 DWI set (and corresponding gradient file with correct naming convention), the dtiRIM will process all files sequentially.

### **Configuration file**
The dtiRIM uses configuration files to control its execution. For more detail on configuration files, please refer to the [EMCqMRI documentation](https://emcqmri.readthedocs.io/en/latest/).

### **Data Format**

Currently the dtiRIM accepts data in two formats: Nifti (**.nii**) and Pickle (**.pkl**).

The tool only process 2D images, with shape convention given by [X, Y, N], where N is the number of weighted images and X,Y are the spatial coordinates of the images.

The dtiRIM also assumes that the first image in the series is a non-diffusion weighted image (S<sub>0</sub>).



# **Training the dtiRIM with my own data**

If you wish to train a dtiRIM model with your own data, you can run the training script:
```console
@user:~$ python3 train_dti_rim.py --configurationFile custom_configuration/train/engine_configuration.txt
```

Here, again, you can specify the path to your data and where to save the model checkpoints through the configuration file, or explicitly through the command line:
```console
@user:~$ python3 train_dti_rim.py --configurationFile custom_configuration/train/engine_configuration.txt --trainingDataPath /PathToMyData --saveCheckpointPath /PathToSaveCheckpoint
```

Any modifications to the dtiRIM configurations can be done in the [inference configuration file](custom_configuration/train/rim_configuration.txt)
## Training data
This dtiRIM implementation only supports 2D data. The convention of naming, format and shape is as described above.

# License
This tool is released under the [Apache License Version 2.0](LICENSE).
